import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store/index';
import axios from 'axios';

import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

Vue.config.productionTip = false;
axios.defaults.baseURL = 'http://localhost:1337/';
Vue.prototype.$http = axios;

new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app');
