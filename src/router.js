import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from './components/features/Home';
import Admin from './components/features/Admin/Admin';
import Bakery from './components/features/User/Shop/Shop/ShopBakery/ShopBakery';
import Grocery from './components/features/User/Shop/Shop/ShopGrocery/ShopGrocery';
import ButcherShop from './components/features/User/Shop/Shop/ShopButcher/ShopButcher';

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  routes: [
    { path: '/', component: Home },
    { path: '/admin', component: Admin },
    { path: '*', redirect: '/' },
    { path: '/boulangerie', component: Bakery },
    { path: '/boucherie', component: ButcherShop },
    { path: '/epicerie', component: Grocery }
  ]
});

export default router;