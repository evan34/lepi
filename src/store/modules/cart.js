const cart = {
  namespaced: true,
  state: {
    data: []
  },
  getters: {
    total(state) {
      return state.data.reduce((acc, p) => {
        acc += p.price
        return acc;
      }, 0);
    }
  },
  mutations: {
    addProduct(state, product) {
      state.data.push(product);
    },
    removeProduct(state, id) {
      const index = state.data.findIndex(d => d.id === id);
      state.data.splice(index, 1);
    }
  }
}

export default cart;