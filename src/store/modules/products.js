import axios from 'axios';

const products = {
  namespaced: true,
  state: {
    data: []
  },
  mutations: {
    addProduct(state, product) {
      state.data.push(product);
    },
    addProducts(state, products) {
      state.data = products;
    }
  },
  actions: {
    getProducts(context) {
      axios.get('products').then(res => {
        const data = res.data;
        context.commit('addProducts', Object.keys(data).map(key => data[key]))
      });
    },
    createProduct(context, product) {
      axios.post('products', product).then(() => {
        context.commit('addProduct', product);
      })
    }
  }
}

export default products;